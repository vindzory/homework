import requests

base_url = 'http://54.90.127.52:8000'

def write_results(message):
    print(message)
    with open('results.txt', 'a') as f:
        if isinstance(message, list):
            for item in message:
                f.write(str(item) + '\n')
        else:
            f.write(str(message) + '\n')  

def get_all_students():
    response = requests.get(f'{base_url}/students')
    write_results("GET all students:")
    write_results(response.json())

def create_student(first_name, last_name, age):
    data = {'first_name': first_name, 'last_name': last_name, 'age': age}
    response = requests.post(f'{base_url}/students', json=data)
    write_results("POST create student:")
    write_results(response.json())

def update_student_age(student_id, new_age):
    data = {'age': new_age}
    response = requests.patch(f'{base_url}/students/{student_id}', json=data)
    write_results("PATCH update student age:")
    write_results(response.json())

def update_student(student_id, first_name, last_name, age):
    data = {'first_name': first_name, 'last_name': last_name, 'age': age}
    response = requests.put(f'{base_url}/students/{student_id}', json=data)
    write_results("PUT update student:")
    write_results(response.json())

def delete_student(student_id):
    response = requests.delete(f'{base_url}/students/{student_id}')
    write_results("DELETE student:")
    write_results(response.json())

if __name__ == '__main__':
    open('results.txt', 'w').close() 

    get_all_students()

    create_student('Петро', 'Канделаки', 20)
    create_student('Ваня', 'Кривой', 22)
    create_student('Елис', 'Купер', 25)

    get_all_students()

    update_student_age(2, 23)

    update_student(3, 'Боб', 'Марли', 30)

    get_all_students()

    delete_student(1)

    get_all_students()
