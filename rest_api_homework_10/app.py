from flask import Flask, jsonify, request 
import csv

app = Flask(__name__)


def read_students():
    with open('students.csv', newline='', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        students = list(reader)
    return students

def write_students(students):
    fieldnames = ['id', 'first_name', 'last_name', 'age']
    with open('students.csv', 'w', newline='', encoding='utf-8') as csvfile:
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(students)

@app.route('/students', methods=['GET'])
def get_students():
    students = read_students()
    if 'id' in request.args:
        student_id = int(request.args['id'])
        student = next((student for student in students if int(student['id']) == student_id), None)
        if student:
            return jsonify(student), 200
        else:
            return jsonify({'error': 'Student not found'}), 404
    elif 'last_name' in request.args:
        last_name = request.args['last_name']
        students_with_last_name = [student for student in students if student['last_name'] == last_name]
        if students_with_last_name:
            return jsonify(students_with_last_name), 200
        else:
            return jsonify({'error': 'Students with last name not found'}), 404
    else:
        return jsonify(students), 200

@app.route('/students', methods=['POST'])
def create_student():
    data = request.json
    if not data or 'first_name' not in data or 'last_name' not in data or 'age' not in data:
        return jsonify({'error': 'Invalid data'}), 400
    students = read_students()
    new_student = {
        'id': len(students) + 1,
        'first_name': data['first_name'],
        'last_name': data['last_name'],
        'age': data['age']
    }
    students.append(new_student)
    write_students(students)
    return jsonify(new_student), 201

@app.route('/students/<int:id>', methods=['PUT'])
def update_student(id):
    data = request.json
    if not data or 'first_name' not in data or 'last_name' not in data or 'age' not in data:
        return jsonify({'error': 'Invalid data'}), 400
    students = read_students()
    student = next((student for student in students if int(student['id']) == id), None)
    if student:
        student.update(data)
        write_students(students)
        return jsonify(student), 200
    else:
        return jsonify({'error': 'Student not found'}), 404

@app.route('/students/<int:id>', methods=['PATCH'])
def update_student_age(id):
    data = request.json
    if not data or 'age' not in data:
        return jsonify({'error': 'Invalid data'}), 400
    students = read_students()
    student = next((student for student in students if int(student['id']) == id), None)
    if student:
        student['age'] = data['age']
        write_students(students)
        return jsonify(student), 200
    else:
        return jsonify({'error': 'Student not found'}), 404

@app.route('/students/<int:id>', methods=['DELETE'])
def delete_student(id):
    students = read_students()
    index = next((i for i, student in enumerate(students) if int(student['id']) == id), None)
    if index is not None:
        deleted_student = students.pop(index)
        write_students(students)
        return jsonify({'message': 'Student deleted', 'student': deleted_student}), 200
    else:
        return jsonify({'error': 'Student not found'}), 404

if __name__ == '__main__':
    app.run(debug=True)
